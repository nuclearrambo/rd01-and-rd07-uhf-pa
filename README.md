ANUHF097C_Brd

Power amplifier board based on two Mitsubishi transistors: RD01MUS2 and RD07MUS2B to give a total power output of 39dBm.
The boards requires a VDD of 7V to 12V and a Vgs between 2.7V to 4V. A Vgs of 3.3V is sufficient to keep the amplifier in class A mode of operation. 

This amplifier works best with a small heat sink underneath the board attached with screws from the top. Added air flow with a small fan will always prove to be beneficial for long operations. 

![PCB image](PCB.png)